package com.example;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;

@RestController
public class CustomerController {

    @Autowired
    private CustomerRepository customerRepository;

    //----------Retrieve All Customers----------//
    @RequestMapping(value = "/customer", method = RequestMethod.GET)
    public List<Customer> getAll() {
        return (List<Customer>) customerRepository.findAll();
    }

    //----------Retrieve Single Customer----------//
    @RequestMapping(value = "/customer/{id}", method = RequestMethod.GET)
    public Customer getOne(@PathVariable("id") long id) {
        Customer customer = customerRepository.findOne(1L);
        return customer;
    }

    //----------Create a Customer----------//
    @RequestMapping(value = "/customer", method = RequestMethod.POST)
    public Customer create(@RequestBody Customer customer) {
        return customerRepository.save(customer);
    }

    //----------Update a Customer----------//
    @RequestMapping(value = "/customer/{id}", method=RequestMethod.PUT)
    public Customer update(@PathVariable String id, @RequestBody Customer customer) {
        Customer update = customerRepository.findOne(1L);
        update.setId(customer.getId());
        update.setFirstName(customer.getFirstName());
        update.setLastName(customer.getLastName());
        return customerRepository.save(update);
    }

    //----------Delete a Customer----------//
    @RequestMapping(value = "/customer/{id}", method=RequestMethod.DELETE)
    public void delete(@PathVariable String id) {

    }
}

@ResponseStatus(HttpStatus.NOT_FOUND)
class UserNotFoundException extends RuntimeException {

    public UserNotFoundException(String userId) {
        super("could not find user '" + userId + "'.");
    }
}