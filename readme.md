# Challenge Notes

## Key Terms

`Long`
Long integer

`int`
Short integer

`Private`
Locally scoped (unusable elsewhere)
*Think: JavaScript locally scoped var*

`Public`
Can be called from anywhere
*Think: JavaScript globally scoped var*

`Final`
Keyword to variable to prevent it from being assigned to something else
*Think: JavaScript const*

`Static`
Means method() doesn't belong to a specific object

`Void`
Means that method() doesn't return a value

`args`
Name of the String[] within the body of main(). "Args" is not special and could be named anything. 

`String[]`
Means an array of String

`main`
A static object, the name of a function. This means the method is part of its class and not part of objects. `main()` is special because it is the start of the program.

`this`
*this* is a reference to the *current object* — the object whose method or constructor is being called

__Pojo__
Plain Old Java Object

__JPA Entity__
A POJO (Plain Old Java Object) class, i.e. an ordinary Java class that is marked (annotated) as having the ability to represent objects in the database.



## Primary Challenge Hurdles

__Tooling__
  * Synchronizing (refresh) Structure of Gradle and IntelliJ IDEA Projects

__CLI Issues__
  * `spring` Rails RVM command
    - Spring gem happens to use executable called `spring`. Released around the same period in time, presumable unintended conflict

__Compilation Errors__
  * e.g. List<Customer>. Iterables conflicts not seen in demo examples
  * Incompatible Types
      - e.g. String cannot be converted to Long `Customer update = customerRepository.findOne(id)`
